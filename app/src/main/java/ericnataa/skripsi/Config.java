package ericnataa.skripsi;


import android.os.Environment;

public class Config {
    public static double s0 = 0.5;
    public static double s1 = -0.5;
    public static double f0 = 0.5;
    public static double f1 = 0.5;
    public static int RESULT_LOAD_IMAGE_REQUEST_CODE = 18;
    public static int RESULT_LOAD_HAAR_IMAGE_REQUEST_CODE = 97;
    public static String EXTENSION =".haar";
    public static String WAVELET_STORAGE ="storage/Wavelet";
    public static String PATH_KOMPRESI = Environment.getExternalStorageDirectory() +"/Wavelet";
    public static String PATH_FILE = "/sdcard/Wavelet/";
    public static String TMP_KOMPRESI = "tmp.png";

}
