package ericnataa.skripsi.algorithm;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import ericnataa.skripsi.Config;
import ericnataa.skripsi.ch.KompresInterface;
import ericnataa.skripsi.utilities.Utility;

/**
 * Created by Ericnataa on 23/01/2018.
 */

public class Haar {
    KompresInterface callback;
    Activity activity;

    public void FWT(double[] data) {
        double[] temp = new double[data.length];
        int h = data.length >> 1;
        for (int i = 0; i < h; i++) {
            int k = (i << 1);
            temp[i] = data[k] * Config.f0 + data[k + 1] * Config.f1;
            temp[i + h] = data[k] * Config.s0 + data[k + 1] * Config.s1;
        }

        for (int i = 0; i < data.length; i++)
            data[i] = temp[i];
    }


    public void FWT(double[][] data) {
        int levRows = data.length;
        int levCols = data[0].length;
        double[] row;
        double[] col;

            row = new double[levCols];
            for (int i = 0; i < levRows; i++)
            {
                for (int j = 0; j < row.length; j++)
                    row[j] = data[i][j];

                FWT(row);

                for (int j = 0; j < row.length; j++)
                    data[i][j] = row[j];
            }


            col = new double[levRows];
            for (int j = 0; j < levCols; j++)
            {
                for (int i = 0; i < col.length; i++)
                    col[i] = data[i][j];

                FWT(col);

                for (int i = 0; i < col.length; i++)
                    data[i][j] = col[i];
            }

    }



    public void kompres(final Bitmap img, final KompresInterface callback, Activity a){
        this.callback = callback;
        try {


        int wImg = img.getWidth();
        int hImg = img.getHeight();

        double[][] Red = new double[wImg][hImg];
        double[][] Green = new double[wImg][hImg];
        double[][] Blue = new double[wImg][hImg];
        int c;
        for (int j = 0; j < hImg; j++) {
            for (int i = 0; i < wImg; i++) {
                c = img.getPixel(i, j);
                Red[i][j] = (double) Utility.getInstance().Scale(0, 255, -1, 1, (c >> 16) & 0x000000FF);
                Green[i][j] = (double) Utility.getInstance().Scale(0, 255, -1, 1, (c >> 8) & 0x000000FF);
                Blue[i][j] = (double) Utility.getInstance().Scale(0, 255, -1, 1, (c) & 0x000000FF);
            }
        }

        byte[] w = (String.valueOf(wImg)+"rgb").getBytes();
        byte[] h = (String.valueOf(hImg)+"rgb").getBytes();
        byte[] combinedWH = new byte[ w.length+h.length];
        System.arraycopy(w,0,combinedWH,0         ,w.length);
        System.arraycopy(h,0,combinedWH,w.length,h.length);

        FWT(Red);
        FWT(Green);
        FWT(Blue);
        Bitmap img3 = Bitmap.createBitmap(wImg, hImg, Bitmap.Config.ARGB_8888);

        for (int j = 0; j < hImg; j++) {
            for (int i = 0; i < wImg; i++) {
                // lh, hl, hh di bulatkan
                if (j > (hImg / 2) - 1) {
                    Red[i][j] = ((int) Utility.getInstance().Scale(-1, 1, 0, 255, (int)Math.round(Red[i][j]))) ;// red component 0...255
                    Green[i][j] = ((int) Utility.getInstance().Scale(-1, 1, 0, 255,  (int)Math.round(Green[i][j])));// green component 0...255
                    Blue[i][j] = (int) Utility.getInstance().Scale(-1, 1, 0, 255,  (int)Math.round(Blue[i][j]));// blue component 0...255
                }else {
                    if (i > (wImg / 2) - 1) {
                        Red[i][j] = ((int) Utility.getInstance().Scale(-1, 1, 0, 255, (int)Math.round(Red[i][j]))) ;// red component 0...255
                        Green[i][j] = ((int) Utility.getInstance().Scale(-1, 1, 0, 255,  (int)Math.round(Green[i][j])));// green component 0...255
                        Blue[i][j] = (int) Utility.getInstance().Scale(-1, 1, 0, 255,  (int)Math.round(Blue[i][j]));// blue component 0...255
                    } else {
                        Red[i][j] = ((int) Utility.getInstance().Scale(-1, 1, 0, 255, Red[i][j])) ;// red component 0...255
                        Green[i][j] = ((int) Utility.getInstance().Scale(-1, 1, 0, 255, Green[i][j]));// green component 0...255
                        Blue[i][j] = (int) Utility.getInstance().Scale(-1, 1, 0, 255, Blue[i][j]);// blue component 0...255
                    }
                }
                img3.setPixel(i,j, Color.rgb((int) Red[i][j], (int)Green[i][j], (int)Blue[i][j]));
            }
        }

        List<Integer> listR=null;
        List<Integer> listG=null;
        List<Integer> listB=null;

        ByteArrayOutputStream listrleR = new ByteArrayOutputStream();
        ByteArrayOutputStream listrleG= new ByteArrayOutputStream();
        ByteArrayOutputStream listrleB= new ByteArrayOutputStream();
        for (int i = 0; i <= Red.length; i++) {



            if(listR !=null){
                getRunLength(listR,listrleR);
            }
            if(listG !=null){
                getRunLength(listG,listrleG);
            }
            if(listB !=null){
                getRunLength(listB,listrleB);
            }

            if(i<Red.length){
                listR = new ArrayList<Integer>();
                listG = new ArrayList<Integer>();
                listB = new ArrayList<Integer>();
                for (int j = 0; j < Red[i].length; j++) {

                    // tiny change 2: actually store the values
                    listR.add((int)Red[i][j]);
                    listG.add((int)Green[i][j]);
                    listB.add((int)Blue[i][j]);
                }
            }

        }


        listrleR.write(String.valueOf("rgb").getBytes());
        listrleG.write(String.valueOf("rgb").getBytes());


        byte[] combinedWHR = new byte[combinedWH.length+listrleR.toByteArray().length];

        System.arraycopy(combinedWH,0,combinedWHR,0         ,combinedWH.length);
        System.arraycopy(listrleR.toByteArray(),0,combinedWHR,combinedWH.length,listrleR.toByteArray().length);


        byte[] combinedGB = new byte[ listrleG.toByteArray().length+listrleB.toByteArray().length];

        System.arraycopy(listrleG.toByteArray(),0,combinedGB,0         ,listrleG.toByteArray().length);
        System.arraycopy(listrleB.toByteArray(),0,combinedGB,listrleG.toByteArray().length,listrleB.toByteArray().length);


        final byte[] combinedWHRGB = new byte[ combinedWHR.length+combinedGB.length];
        System.arraycopy(combinedWHR,0,combinedWHRGB,0         ,combinedWHR.length);
        System.arraycopy(combinedGB,0,combinedWHRGB,combinedWHR.length,combinedGB.length);
            a.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    callback.informasiKompres(combinedWHRGB);
                }
            });

        }catch (Exception io) {
            io.printStackTrace();

            a.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    callback.error("Proses gagal");
                }
            });

        }
    }


    public void getRunLength(List<Integer> imageByteArray,ByteArrayOutputStream  dest){
        try{

            int lastByte = imageByteArray.get(0);
            int matchCount = 1;
            int maxcount = 255;

            for(int i=1; i < imageByteArray.size(); i++){
                int thisByte = imageByteArray.get(i);
                if (lastByte == thisByte) {
                    matchCount++;
                }
                else {

                    if(matchCount >= maxcount){
                        int  sisa= matchCount % maxcount;
                        for (int j = maxcount; j <=(matchCount - sisa); j+=maxcount){
                            dest.write((byte)lastByte);
                            dest.write((byte)maxcount);
                        }

                        if(sisa!=0){
                            dest.write((byte)lastByte);
                            dest.write((byte)sisa);

                        }

                    }else{
                        dest.write((byte)lastByte);
                        dest.write((byte)matchCount);
                    }


                    matchCount=1;
                    lastByte = thisByte;
                }
            }
            if(matchCount >= maxcount){
                int  sisa= matchCount % maxcount;
                for (int j = maxcount; j <=(matchCount - sisa); j+=maxcount){
                    dest.write((byte)lastByte);
                    dest.write((byte)maxcount);
                }

                if(sisa!=0){
                    dest.write((byte)lastByte);
                    dest.write((byte)sisa);
                }

            }else{
                dest.write((byte)lastByte);
                dest.write((byte)matchCount);
            }


        }catch (Exception io){
            io.printStackTrace();

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    callback.error("gagal saat melakukan pengurangan pixel");
                }
            });

        }

    }


}
