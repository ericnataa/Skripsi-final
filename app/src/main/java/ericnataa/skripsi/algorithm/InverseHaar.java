package ericnataa.skripsi.algorithm;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;

import java.util.ArrayList;
import java.util.List;

import ericnataa.skripsi.Config;
import ericnataa.skripsi.ch.DekompresInterface;
import ericnataa.skripsi.ch.PerbandinganInterface;
import ericnataa.skripsi.utilities.Utility;

/**
 * Created by Ericnataa on 23/01/2018.
 */

public class InverseHaar {
    DekompresInterface callback;
    PerbandinganInterface bandingCallback;
    Activity activity;

    public void IWT(double[] data) {
        double[] temp = new double[data.length];

        int h = data.length >> 1;
        for (int i = 0; i < h; i++) {
            int k = (i << 1);
            temp[k] = (data[i] * Config.f0 + data[i + h] * Config.s0) / Config.s0;
            temp[k + 1] = (data[i] * Config.f1 + data[i + h] * Config.s1) / Config.f0;
        }

        for (int i = 0; i < data.length; i++)
            data[i] = temp[i];
    }

    public void IWT(double[][] data) {
        int levRows = data.length;
        int levCols = data[0].length;

        double[] col;
        double[] row;



            col = new double[levRows];
            for (int j = 0; j < levCols; j++)
            {
                for (int i = 0; i < col.length; i++)
                    col[i] = data[i][j];

                IWT(col);

                for (int i = 0; i < col.length; i++)
                    data[i][j] = col[i];
            }

            row = new double[levCols];
            for (int i = 0; i < levRows; i++)
            {
                for (int j = 0; j < row.length; j++)
                    row[j] = data[i][j];

                IWT(row);

                for (int j = 0; j < row.length; j++)
                    data[i][j] = row[j];
            }

    }

    public void dekompres(byte[] bytes, final DekompresInterface callback, Activity a, final PerbandinganInterface bandingCallback){
        this.callback =callback;
        this.bandingCallback=bandingCallback;
        activity = a;

        try {


            String byteString = new String(bytes,  "ISO-8859-1");
            String[] separated = byteString.split("rgb");
            int width = Integer.parseInt(separated[0]);
            int height = Integer.parseInt(separated[1]);
            double[][] Red2= new double[width][height];
            double[][] Green2 = new double[width][height];
            double[][] Blue2 = new double[width][height];

            List<Integer> dekompres=new ArrayList<>();


            getDecodeRunLength(separated[2].getBytes("ISO-8859-1"),dekompres);
            for (int j = 0; j < height; j++) {
                for (int i = 0; i < width; i++) {
                    Red2[i][j] = (double) Utility.getInstance().Scale(0, 255, -1, 1, (dekompres.get(height*i+j)));
                }
            }

            dekompres=new ArrayList<>();
            getDecodeRunLength(separated[3].getBytes("ISO-8859-1"),dekompres);
            for (int j = 0; j < height; j++) {
                for (int i = 0; i < width; i++) {
                    Green2[i][j] = (double) Utility.getInstance().Scale(0, 255, -1, 1, (dekompres.get(height*i+j)));
                }
            }

            dekompres=new ArrayList<>();
            getDecodeRunLength(separated[4].getBytes("ISO-8859-1"),dekompres);





            for (int j = 0; j < height; j++) {
                for (int i = 0; i < width; i++) {
                    Blue2[i][j] = (double) Utility.getInstance().Scale(0, 255, -1, 1, (dekompres.get(height*i+j)));
                }
            }


            IWT(Red2);
            IWT(Green2);
            IWT(Blue2);

           final  Bitmap img2 = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

            for (int j = 0; j < height; j++) {
                for (int i = 0; i < width; i++) {
                    int r1 = ((int) Utility.getInstance().Scale(-1, 1, 0, 255, Red2[i][j])) ;// red component 0...255
                    int g1 = ((int) Utility.getInstance().Scale(-1, 1, 0, 255, Green2[i][j]));// green component 0...255
                    int b1 = (int) Utility.getInstance().Scale(-1, 1, 0, 255, Blue2[i][j]);// blue component 0...255

                    img2.setPixel(i,j, Color.rgb(r1,g1,b1));
                }
            }
            dekompres.clear();

            a.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(callback!=null){
                        callback.informasiDekompres(img2);

                    }

                    if(bandingCallback!=null){
                        bandingCallback.informasiDekompres(img2);

                    }

                }
            });


        }catch (Exception io){
            io.printStackTrace();

            a.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(callback!=null){
                        callback.error("Proses gagal");

                    }

                    if(bandingCallback!=null){
                        bandingCallback.error("Proses gagal");

                    }

                }
            });
        }
    }

    public void getDecodeRunLength(byte[] rle, List<Integer> array) {
        try {

            int k = 0;
            for (int i = 1; i < rle.length; i += 2) {

                int count = rle[i] & 0x000000FF;
                for (int j = 0; j < count; j++) {
                    array.add(rle[i - 1] & 0x000000FF);
                }
                k += count;

            }


        } catch (Exception io) {
            io.printStackTrace();

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(callback!=null){
                        callback.error("gagal saat melakukan pengembalian pixel");

                    }

                    if(bandingCallback!=null){
                        bandingCallback.error("gagal saat melakukan pengembalian pixel");

                    }

                }
            });
        }
    }
}
