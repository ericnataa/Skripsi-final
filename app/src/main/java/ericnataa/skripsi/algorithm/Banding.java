package ericnataa.skripsi.algorithm;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;

import java.util.ArrayList;
import java.util.List;

import ericnataa.skripsi.ch.PerbandinganInterface;
import ericnataa.skripsi.model.BandingData;

/**
 * Created by User on 1/25/2018.
 */

public class Banding {
    PerbandinganInterface callback;
    Activity activity;
    List<BandingData> bandingDataList;
    public void kompres(final Bitmap imgAsli, Bitmap imgHaar, final PerbandinganInterface callback, Activity a) {
        this.callback = callback;
        this.activity = activity;
        bandingDataList = new ArrayList<BandingData>();
        try {


        int wImg = imgAsli.getWidth();
        int hImg = imgAsli.getHeight();
        int pixelAsli;
        int pixelHaar;

        for (int j = 0; j < hImg; j++) {
            for (int i = 0; i < wImg; i++) {
                pixelAsli = imgAsli.getPixel(i, j);
                pixelHaar = imgHaar.getPixel(i, j);
                int redValue =(pixelAsli >> 16) & 0x000000FF;
                int blueValue = (pixelAsli >> 8) & 0x000000FF;
                int greenValue = (pixelAsli) & 0x000000FF;
                int redValueH = (pixelHaar >> 16) & 0x000000FF;
                int blueValueH =(pixelHaar >> 8) & 0x000000FF;
                int greenValueH = (pixelHaar) & 0x000000FF;
                if(redValue!=redValueH || blueValue!=blueValueH || greenValue!=greenValueH){

                    if(bandingDataList.size()>20){
                        break;
                    }
                    BandingData data = new BandingData();
                    data.setX(String.valueOf(i));
                    data.setY(String.valueOf(j));
                    if(redValue!=redValueH ){
                    data.setRedAsli(String.valueOf(redValue));
                    data.setRedHaar(String.valueOf(redValueH));

                    }


                    if(blueValue!=blueValueH){

                        data.setBlueAsli(String.valueOf(blueValue));
                        data.setBlueHaar(String.valueOf(blueValueH));
                    }

                    if(greenValue!=greenValueH){
                        data.setGreenAsli(String.valueOf(greenValue));
                        data.setGreenHaar(String.valueOf(greenValueH));
                    }

                    bandingDataList.add(data);

                }



            }
        }


            a.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    callback.hasilPerbandingan(bandingDataList);
                }
            });
        }catch (Exception io) {
            io.printStackTrace();

            a.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    callback.error("Proses gagal");
                }
            });
        }
    }
}
