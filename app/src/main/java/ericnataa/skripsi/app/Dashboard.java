package ericnataa.skripsi.app;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import java.util.ArrayList;
import java.util.List;

import ericnataa.skripsi.fragment.Dekompresi;
import ericnataa.skripsi.fragment.Kompresi;
import ericnataa.skripsi.fragment.Perbandingan;
import ericnataa.skripsi.fragment.Profile;

/**
 * Created by Ericnataa on 23/01/2018.
 */

public class Dashboard extends AppCompatActivity {
    CharSequence[] Titles;
    ViewPager viewPager;
    FloatingActionsMenu fabmenu;
    FloatingActionButton fabkompres, simpan;
    int position;
    //fragment
    Kompresi kompresi;
    Dekompresi dekompresi;
    Perbandingan perbandingan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        List<String> listItems = new ArrayList<String>();
        listItems.add("Kompresi");
        listItems.add("Dekompresi");
        listItems.add("Perbandingan");
        listItems.add("Profil");

        Titles = listItems.toArray(new CharSequence[listItems.size()]);
         PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), Titles);
        viewPager = findViewById(R.id.viewpager);
        fabmenu = findViewById(R.id.fabmenu);
        simpan = findViewById(R.id.fabSimpan);
        fabkompres = findViewById(R.id.fabkompres);

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);
        TabLayout tabLayout = findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //do stuff here
                position = tab.getPosition();
                if(tab.getPosition()==0){
                    visibleMenu(View.VISIBLE);
                    collapse();

                }else if(tab.getPosition()==1){
                    visibleMenu(View.GONE);
                    collapse();
                }else if(tab.getPosition()==2){
                    visibleMenu(View.GONE);

                }else{
                    visibleMenu(View.GONE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        fabkompres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                kompresi.doKompres();
            }
        });

        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    if(kompresi.simpanFile()){
                        dialogSave(kompresi.getImageName());
                    }


            }
        });

    }
    public  void dialogSave(String imagename){
        AlertDialog.Builder builder = new AlertDialog.Builder(Dashboard.this);
        final View alertView = View.inflate(new ContextThemeWrapper(Dashboard.this, R.style.AlertDialog_AppCompat), R.layout.dialog_path, null);
        final EditText nama_file = (EditText) alertView.findViewById(R.id.nama_file);
        nama_file.setText(imagename);

        builder.setView(alertView);
        builder.setTitle(null);
        // Set the action buttons
        builder.setPositiveButton("Simpan", null);
        builder.setNegativeButton("Batal", null);
        final AlertDialog mAlertDialog = builder.create();

        mAlertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {

                Button b = mAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // TODO Do something
                        String namafile = nama_file.getText().toString();

                        if (nama_file.length() == 0) {
                            nama_file.setError("nama file tidak boleh kosong");

                        } else {

                            kompresi.simpanCode(namafile);


                            mAlertDialog.dismiss();
                            collapse();
                        }


                    }
                });
            }
        });
        mAlertDialog.show();

    }

    private void visibleMenu(int menu){
        fabmenu.setVisibility(menu);
    }
    public void collapse(){
        fabmenu.collapse();
    }

     class PagerAdapter extends FragmentStatePagerAdapter {
        CharSequence Titles[];
        public PagerAdapter(FragmentManager fm, CharSequence mTitles[]) {
            super(fm);
            this.Titles = mTitles;


        }

        @Override
        public Fragment getItem(int position) {

            if(position == 0)
            {
                kompresi= new Kompresi();
                return kompresi;
            }
            else if(position == 1)
            {
                dekompresi = new Dekompresi();
                return dekompresi;
            }
            else if (position == 2)
            {

                perbandingan = new Perbandingan();
                return perbandingan;
            } else{

                return new Profile();
            }


        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }


        @Override
        public int getCount() {
            return Titles.length;
        }
    }
}
