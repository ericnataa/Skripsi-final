package ericnataa.skripsi.ch;

import android.graphics.Bitmap;

/**
 * Created by User on 1/25/2018.
 */

public interface DekompresInterface {

    void informasiDekompres(Bitmap gambar);
    void error(String error);
}
