package ericnataa.skripsi.ch;

/**
 * Created by Ericnataa on 24/01/2018.
 */

public interface KompresInterface {

    void informasiKompres(byte[] gambar);
    void error(String error);
}
