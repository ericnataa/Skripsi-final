package ericnataa.skripsi.ch;

import android.graphics.Bitmap;

import java.util.List;

import ericnataa.skripsi.model.BandingData;

/**
 * Created by User on 1/25/2018.
 */

public interface PerbandinganInterface {
    void informasiDekompres(Bitmap gambar);
    void hasilPerbandingan(List<BandingData> data);
    void error(String error);
}

