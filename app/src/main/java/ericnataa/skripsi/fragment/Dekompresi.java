package ericnataa.skripsi.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DecimalFormat;

import ericnataa.skripsi.Config;
import ericnataa.skripsi.algorithm.InverseHaar;
import ericnataa.skripsi.app.Dashboard;
import ericnataa.skripsi.app.R;
import ericnataa.skripsi.ch.DekompresInterface;
import ericnataa.skripsi.utilities.Utility;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Ericnataa on 23/01/2018.
 */

public class Dekompresi extends Fragment {
    TextView lokasi, format,ukuran,waktu,
    panjang,lebar,ukuran_dekompres;

    ImageView gmbar;
    long timestart=0;
    RelativeLayout chooseImg;
    String imageName="";
    AlertDialog dialog;
    Dashboard dashboard;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dekompresi, container, false);
        setHasOptionsMenu(true);
        dashboard = ((Dashboard)getActivity());
        dialog = Utility.getInstance().dialogLoading(getActivity());
        gmbar = view.findViewById(R.id.gmbar);
        format = view.findViewById(R.id.format);
        waktu = view.findViewById(R.id.waktu);
        ukuran = view.findViewById(R.id.ukuran);
        lokasi = view.findViewById(R.id.lokasi);
        panjang = view.findViewById(R.id.panjang);
        lebar = view.findViewById(R.id.lebar);
        chooseImg = view.findViewById(R.id.chooseImg);
        ukuran_dekompres = view.findViewById(R.id.ukuran_dekompres);
        chooseImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFolder();
            }
        });
        gmbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFolder();
            }
        });
        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.reset, menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.reset) {
            reset();

            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    public void openFolder(){

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {


            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    0);



        } else {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath());
            intent.setDataAndType(uri, "*/*");
            startActivityForResult(Intent.createChooser(intent, "Open folder"), Config.RESULT_LOAD_HAAR_IMAGE_REQUEST_CODE);

        }

    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == Config.RESULT_LOAD_HAAR_IMAGE_REQUEST_CODE && resultCode == RESULT_OK && null != data) {

            Uri selectedImageUri = data.getData();
            String imagepath = Utility.getInstance().getPathHaar(getActivity(),selectedImageUri);
            if(imagepath!=null){
                imageName = imagepath.substring(imagepath.lastIndexOf("/"));
                String frmt = imageName.substring(imageName.lastIndexOf("."));

                if(!frmt.equals(Config.EXTENSION)){
                    Toast.makeText(getActivity(),"Hanya dapat mengload gambar yang memilik ekstension "+Config.EXTENSION,Toast.LENGTH_LONG).show();
                }else{

                    File file = new File(imagepath);

                    int size = (int) file.length();
                   byte [] gambarB = new byte[size];
                    try {
                        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                        buf.read(gambarB, 0, gambarB.length);
                        buf.close();
                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    doInvHaar(gambarB);
                    setInfo(imagepath,frmt,file.length());

                }

            }


        }
    }

    DekompresInterface callback = new DekompresInterface() {
        @Override
        public void informasiDekompres(Bitmap gambar) {
            setInfoDekompres(gambar);
            dialog.dismiss();
            dashboard.collapse();
        }

        @Override
        public void error(String error) {

            Toast.makeText(getContext(),error,Toast.LENGTH_SHORT).show();
            dialog.dismiss();
        }
    };


    private void doInvHaar(final byte[] gambarB){
        dialog.show();
        timestart = System.currentTimeMillis();
        Thread thread = new Thread() {
            @Override
            public void run() {
                try{
                InverseHaar invHaarKompresi = new InverseHaar();

                invHaarKompresi.dekompres(gambarB, callback,getActivity(),null);
                }catch (OutOfMemoryError io){
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(), "silahkan restart aplikasi terlebih dahulu", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    });
                }
            }
        };

        thread.start();

    }

    private void setVisible(int gbr, int choose){
        gmbar.setVisibility(gbr);
        chooseImg.setVisibility(choose);
    }

    private void setInfo(String lk, String frmt, long size){
        lokasi.setText(lk);
        format.setText(frmt);
        ukuran.setText(Utility.getInstance().convertByte(size));
    }

    private void setInfoDekompres(Bitmap bmp){
        long timestop = System.currentTimeMillis() - timestart;
        gmbar.setImageBitmap(bmp);
        setVisible(View.VISIBLE,View.GONE);
        panjang.setText(Utility.getInstance().convertDot(String.valueOf(bmp.getHeight())));
        lebar.setText(Utility.getInstance().convertDot(String.valueOf(bmp.getWidth())));
        ukuran_dekompres.setText(Utility.getInstance().convertByte(bmp.getHeight()*bmp.getWidth()*3));

        String d = String.valueOf(new Timestamp(timestop));
        String[] e = d.split(" ")[1].split(":");

        if(e[1].equals("00")){
            waktu.setText(String.valueOf(Double.parseDouble(e[2]))+" detik");
        }else{

            int gd = 60 * Integer.parseInt(e[1]);
            double gd2 = gd +  Double.parseDouble(e[2]);
            DecimalFormat df = new DecimalFormat("#.###");
            waktu.setText(String.valueOf(df.format(gd2)+" detik"));
        }

    }

    private void reset(){
        lokasi.setText("-");
        format.setText("-");
        ukuran.setText("-");
        panjang.setText("-");
        lebar.setText("-");
        waktu.setText("-");
        timestart=0;
        ukuran_dekompres.setText("-");
        setVisible(View.GONE,View.VISIBLE);

       imageName="";
    }


}