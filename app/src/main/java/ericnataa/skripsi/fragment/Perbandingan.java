package ericnataa.skripsi.fragment;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

import ericnataa.skripsi.Config;
import ericnataa.skripsi.algorithm.Banding;
import ericnataa.skripsi.algorithm.InverseHaar;
import ericnataa.skripsi.app.Dashboard;
import ericnataa.skripsi.app.R;
import ericnataa.skripsi.ch.PerbandinganInterface;
import ericnataa.skripsi.model.BandingData;
import ericnataa.skripsi.utilities.Utility;

import static android.app.Activity.RESULT_OK;

/**
 * Created by User on 1/24/2018.
 */

public class Perbandingan extends Fragment {
    ImageView gmbar;
    TextView panjang,
            lebar,
            lokasi,
            format,
            ukuran;

    RelativeLayout chooseImg;

    ImageView gmbar_dekom;
    TextView panjang_dekom,
            lebar_dekom,
            lokasi_dekom,
            format_dekom,
            ukuran_dekom;
    RecyclerView banding_list;
    RelativeLayout chooseImg_dekom;
    byte[] gambarB=null;
    Bitmap bmpAsli;
    Bitmap bmpHaar;
    AlertDialog dialog;
    Dashboard dashboard;
    Button proses;
    TextView gmbr_sama;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.perbandingan, container, false);
        setHasOptionsMenu(true);
        dialog = Utility.getInstance().dialogLoading(getActivity());
        dashboard = ((Dashboard)getActivity());
        gmbar = view.findViewById(R.id.gmbar);
        chooseImg = view.findViewById(R.id.chooseImg);
        panjang = view.findViewById(R.id.panjang);
        lebar = view.findViewById(R.id.lebar);
        lokasi = view.findViewById(R.id.lokasi);
        ukuran = view.findViewById(R.id.ukuran);
        format = view.findViewById(R.id.format);
        gmbar_dekom = view.findViewById(R.id.gmbar_dekom);
        chooseImg_dekom = view.findViewById(R.id.chooseImg_dekom);
        panjang_dekom = view.findViewById(R.id.panjang_dekom);
        lebar_dekom = view.findViewById(R.id.lebar_dekom);
        lokasi_dekom = view.findViewById(R.id.lokasi_dekom);
        ukuran_dekom = view.findViewById(R.id.ukuran_dekom);
        format_dekom = view.findViewById(R.id.format_dekom);
        proses = view.findViewById(R.id.proses);
        banding_list = view.findViewById(R.id.banding_list);
        gmbr_sama = view.findViewById(R.id.gmbr_sama);

        gmbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallerry();
            }
        });
        chooseImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallerry();
            }
        });
        gmbar_dekom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFolder();
            }
        });
        chooseImg_dekom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFolder();
            }
        });

        proses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bmpAsli!=null && bmpHaar!=null){

                    if(bmpAsli.getWidth()!=bmpHaar.getWidth()){
                        Toast.makeText(getActivity(),"lebar keduaa gambar tidak sama",Toast.LENGTH_LONG).show();

                        return;
                    }


                    if(bmpAsli.getHeight()!=bmpHaar.getHeight()){
                        Toast.makeText(getActivity(),"panjang keduaa gambar tidak sama",Toast.LENGTH_LONG).show();

                        return;
                    }

                    doProses();
                }else{
                    Toast.makeText(getActivity(),"Silahkan upload kedua gambar terlebih dahulu",Toast.LENGTH_LONG).show();

                }
            }
        });
        return view;
    }

    private void doProses(){
        dialog.show();
        Thread thread = new Thread() {
            @Override
            public void run() {
                Banding banding = new Banding();

                banding.kompres(bmpAsli,bmpHaar, callback,getActivity());

            }
        };

        thread.start();
    }
    public void openFolder(){

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?


            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    0);

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.

        } else {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath());
            intent.setDataAndType(uri, "*/*");
            startActivityForResult(Intent.createChooser(intent, "Open folder"), Config.RESULT_LOAD_HAAR_IMAGE_REQUEST_CODE);

        }

    }
    private void openGallerry(){
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?


            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    0);
            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.

        } else {
            intentGallery();
        }

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == Config.RESULT_LOAD_IMAGE_REQUEST_CODE && resultCode == RESULT_OK && null != data) {
            //Bitmap photo = (Bitmap) data.getData().getPath();
            Uri selectedImageUri = data.getData();
            String imagepath = Utility.getInstance().getPath(selectedImageUri,getActivity());
            String imageName = imagepath.substring(imagepath.lastIndexOf("/"));
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmpAsli = BitmapFactory.decodeFile(imagepath, bmOptions);
            File file = new File(imagepath);
            long ukuranGambarAsli = file.length();
            String frmt = imageName.substring(imageName.lastIndexOf("."));
            String p = String.valueOf(bmpAsli.getHeight());
            String l = String.valueOf(bmpAsli.getWidth());
            setImageOriInformation(p,l,imagepath,frmt,ukuranGambarAsli);
            visibleImage(View.VISIBLE,View.GONE);
        } else  if (requestCode == Config.RESULT_LOAD_HAAR_IMAGE_REQUEST_CODE && resultCode == RESULT_OK && null != data) {
            //Bitmap photo = (Bitmap) data.getData().getPath();
            Uri selectedImageUri = data.getData();
            String imagepath = Utility.getInstance().getPathHaar(getActivity(),selectedImageUri);
            if(imagepath!=null){
                String imageName = imagepath.substring(imagepath.lastIndexOf("/"));
                String frmt = imageName.substring(imageName.lastIndexOf("."));

                if(!frmt.equals(Config.EXTENSION)){
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmpHaar = BitmapFactory.decodeFile(imagepath, bmOptions);
                    File file = new File(imagepath);
                    long ukuranGambarAsli = file.length();
                    String p = String.valueOf(bmpAsli.getHeight());
                    String l = String.valueOf(bmpAsli.getWidth());
                    setInfo(imagepath,frmt,file.length());
                    setInfoDekompres(bmpHaar);
//                    Toast.makeText(getActivity(),"Hanya dapat mengload gambar yang memilik ekstension "+Config.EXTENSION,Toast.LENGTH_LONG).show();
                }else{

                    File file = new File(imagepath);

                    int size = (int) file.length();
                     gambarB = new byte[size];
                    try {
                        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                        buf.read(gambarB, 0, gambarB.length);
                        buf.close();
                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    setInfo(imagepath,frmt,file.length());
                    doDekompres();
                }

            }


        }
    }
    public void doDekompres(){
        if(gambarB==null){
            openFolder();
            Toast.makeText(getActivity(),"Silahkan pilih gambar yang ekstension haar terlebih dahulu",Toast.LENGTH_LONG).show();
        }else{
            doInvHaar();
        }
    }
    PerbandinganInterface callback = new PerbandinganInterface() {
        @Override
        public void informasiDekompres(Bitmap gambar) {
            setInfoDekompres(gambar);
            dialog.dismiss();

        }

        @Override
        public void hasilPerbandingan(List<BandingData> hasil) {

            if(hasil.size()==0){
                banding_list.setVisibility(View.GONE);
                gmbr_sama.setVisibility(View.VISIBLE);
            }else{
                gmbr_sama.setVisibility(View.GONE);
                banding_list.setVisibility(View.VISIBLE);
                LinearLayoutManager lm = new LinearLayoutManager(getActivity());
                banding_list.setLayoutManager(lm);
                banding_list.setHasFixedSize(true);
                banding_list.setNestedScrollingEnabled(false);
                BandingAdapter mAdapter = new BandingAdapter(hasil);
                banding_list.setAdapter(mAdapter);
            }
            dialog.dismiss();

        }

        @Override
        public void error(String error) {
            Toast.makeText(getContext(),error,Toast.LENGTH_SHORT).show();
            dialog.dismiss();
        }
    };

    private void setInfoDekompres(Bitmap bmp){
        bmpHaar=bmp;
        gmbar_dekom.setImageBitmap(bmp);
        visibleImageHAAR(View.VISIBLE,View.GONE);



        panjang_dekom.setText(Utility.getInstance().convertDot(String.valueOf(bmp.getHeight())));
        lebar_dekom.setText(Utility.getInstance().convertDot(String.valueOf(bmp.getWidth())));


    }
    private void doInvHaar(){
        dialog.show();
        Thread thread = new Thread() {
            @Override
            public void run() {
                InverseHaar invHaarKompresi = new InverseHaar();

                invHaarKompresi.dekompres(gambarB, null,getActivity(),callback);

            }
        };

        thread.start();

    }

    private void setInfo(String lk, String frmt, long size){
        lokasi_dekom.setText(lk);
        format_dekom.setText(frmt);
        ukuran_dekom.setText(Utility.getInstance().convertByte(size));
    }


    private void setImageOriInformation(String pnjng, String lbr, String path,String formattxt,long ukuranGambarAsli){
        panjang.setText(Utility.getInstance().convertDot(pnjng));
        lebar.setText(Utility.getInstance().convertDot(lbr));
        lokasi.setText(path);
        ukuran.setText(Utility.getInstance().convertByte(ukuranGambarAsli));
        format.setText(formattxt);
        gmbar.setImageBitmap(bmpAsli);
    }


    private void intentGallery(){
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, Config.RESULT_LOAD_IMAGE_REQUEST_CODE);

    }
    private void visibleImageHAAR(int img, int choose){
        gmbar_dekom.setVisibility(img);
        chooseImg_dekom.setVisibility(choose);
    }
    private void visibleImage(int img, int choose){
        gmbar.setVisibility(img);
        chooseImg.setVisibility(choose);
    }

    private void reset(){
        panjang.setText("-");
        lebar.setText("-");
        lokasi.setText("-");
        ukuran.setText("-");
        format.setText("-");
        visibleImage(View.GONE, View.VISIBLE);

        panjang_dekom.setText("-");
        lebar_dekom.setText("-");
        lokasi_dekom.setText("-");
        ukuran_dekom.setText("-");
        format_dekom.setText("-");
        visibleImageHAAR(View.GONE, View.VISIBLE);
        gambarB=null;
        bmpAsli = null;
        bmpHaar = null;
        banding_list.setVisibility(View.GONE);
        gmbr_sama.setVisibility(View.GONE);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.banding_menu, menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id=item.getItemId();
        //noinspection SimplifiableIdStatement
        if(id==R.id.reset) {
            reset();

            return true;
        }else if(id==R.id.info){

            String txt = "Halaman ini berfungsi untuk membuktikan bahwa kompresi citra menggunakan transformasi haar wavelet merupakan kompresi yang bersifat lossy."+"\n"+
            "dalam pembuktian ini peneliti hanya memunculkan maksimal hanya 20 perbedaan warna dari gambar asli dengan gambar yang dikompresi menggunakan transformasi haar wavelet.";
            dialogInfo(txt);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void dialogInfo(String txt){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View alertView = View.inflate(new ContextThemeWrapper(getActivity(), R.style.AlertDialog_AppCompat), R.layout.banding_info, null);
        final TextView infoTxt = (TextView) alertView.findViewById(R.id.infoTxt);
        infoTxt.setText(txt);

        builder.setView(alertView);
        builder.setTitle(null);
        // Set the action buttons
        builder.setNegativeButton("Tutup", null);
        final AlertDialog mAlertDialog = builder.create();
        mAlertDialog.show();

    }

     class BandingAdapter extends RecyclerView.Adapter<BandingAdapter.ViewHolder>{

        private List<BandingData> bandingDataList;

        public BandingAdapter(List<BandingData> bandingDataList) {
            this.bandingDataList = bandingDataList;


        }


         public class ViewHolder extends RecyclerView.ViewHolder {
             public TextView x,y,r_asli,g_asli,b_asli,
                     r_haar,g_haar,b_haar;
                    TableRow r_asli_lay,g_asli_lay,b_asli_lay,r_haar_lay
                            ,g_haar_lay,b_haar_lay;

             public ViewHolder(View itemView) {
                 super(itemView);
                 x = itemView.findViewById(R.id.x);
                 y = itemView.findViewById(R.id.y);
                 r_asli = itemView.findViewById(R.id.r_asli);
                 g_asli = itemView.findViewById(R.id.g_asli);
                 b_asli = itemView.findViewById(R.id.b_asli);
                 r_haar = itemView.findViewById(R.id.r_haar);
                 g_haar = itemView.findViewById(R.id.g_haar);
                 b_haar = itemView.findViewById(R.id.b_haar);
                 r_asli_lay = itemView.findViewById(R.id.r_asli_lay);
                 g_asli_lay = itemView.findViewById(R.id.g_asli_lay);
                 b_asli_lay = itemView.findViewById(R.id.b_asli_lay);
                 r_haar_lay = itemView.findViewById(R.id.r_haar_lay);
                 g_haar_lay = itemView.findViewById(R.id.g_haar_lay);
                 b_haar_lay = itemView.findViewById(R.id.b_haar_lay);

             }
         }

        @Override
        public BandingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.banding_item, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(BandingAdapter.ViewHolder holder, int position) {
            BandingData data = bandingDataList.get(position);

          holder.x.setText(data.getX());
          holder.y.setText(data.getY());

          if(data.getRedAsli()!=null){
                holder.r_asli.setText(data.getRedAsli());
                holder.r_haar.setText(data.getRedHaar());
              holder.r_asli_lay.setVisibility(View.VISIBLE);
              holder.r_haar_lay.setVisibility(View.VISIBLE);
          }else{
              holder.r_asli_lay.setVisibility(View.GONE);
              holder.r_haar_lay.setVisibility(View.GONE);
          }

            if(data.getGreenAsli()!=null){
                holder.g_asli.setText(data.getGreenAsli());
                holder.g_haar.setText(data.getGreenHaar());
                holder.g_asli_lay.setVisibility(View.VISIBLE);
                holder.g_haar_lay.setVisibility(View.VISIBLE);
            }else{
                holder.g_asli_lay.setVisibility(View.GONE);
                holder.g_haar_lay.setVisibility(View.GONE);
            }


            if(data.getBlueAsli()!=null){
                holder.b_asli.setText(data.getBlueAsli());
                holder.b_haar.setText(data.getBlueHaar());
                holder.b_asli_lay.setVisibility(View.VISIBLE);
                holder.b_haar_lay.setVisibility(View.VISIBLE);
            }else{
                holder.b_asli_lay.setVisibility(View.GONE);
                holder.b_haar_lay.setVisibility(View.GONE);
            }
        }

        @Override
        public int getItemCount() {
            return bandingDataList.size();
        }

    }

}
