package ericnataa.skripsi.fragment;

import android.Manifest;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Timestamp;
import java.text.DecimalFormat;

import ericnataa.skripsi.Config;
import ericnataa.skripsi.algorithm.Haar;
import ericnataa.skripsi.app.Dashboard;
import ericnataa.skripsi.app.R;
import ericnataa.skripsi.ch.KompresInterface;
import ericnataa.skripsi.utilities.Utility;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Ericnataa on 23/01/2018.
 */

public class Kompresi extends Fragment {
    ImageView gmbar;
    TextView panjang,
            lebar,
            lokasi,
            ukuran,
            lokasi_kompresi,
            ukuran_kompresi,
            rasio_kompresi,
            waktu_kompresi,
            format,rasio_asli_txt,rasio_asli;
        RelativeLayout zoom,chooseImg;
    Dashboard dashboard;
    String imageName="";
    long ukuranGambarAsli;
    Bitmap bmp;
    AlertDialog dialog;
    long timestart=0;
    byte[] byteHasilKompresi=null;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.kompresi, container, false);
        setHasOptionsMenu(true);
        dialog = Utility.getInstance().dialogLoading(getActivity());
        dashboard = ((Dashboard)getActivity());
         gmbar = view.findViewById(R.id.gmbar);
         chooseImg = view.findViewById(R.id.chooseImg);
         zoom = view.findViewById(R.id.zoom);
         panjang = view.findViewById(R.id.panjang);
         lebar = view.findViewById(R.id.lebar);
         lokasi = view.findViewById(R.id.lokasi);
         ukuran = view.findViewById(R.id.ukuran);
         lokasi_kompresi = view.findViewById(R.id.lokasi_kompresi);
         ukuran_kompresi = view.findViewById(R.id.ukuran_kompresi);
        waktu_kompresi = view.findViewById(R.id.waktu_kompresi);
         rasio_kompresi = view.findViewById(R.id.rasio_kompresi);
        rasio_asli = view.findViewById(R.id.rasio_asli);
        rasio_asli_txt = view.findViewById(R.id.rasio_asli_txt);
        format = view.findViewById(R.id.format);

        gmbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallerry();
            }
        });
         chooseImg.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                openGallerry();
             }
         });
        return view;
    }

    private void openGallerry(){
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    0);

        } else {
            intentGallery();
        }

    }

    KompresInterface callback = new KompresInterface() {
        @Override
        public void informasiKompres(byte[] gambar) {
            long timestop = System.currentTimeMillis() - timestart;
            String d = String.valueOf(new Timestamp(timestop));
            String[] e = d.split(" ")[1].split(":");

            if(e[1].equals("00")){
                waktu_kompresi.setText(String.valueOf(Double.parseDouble(e[2]))+" detik");
            }else{

                int gd = 60 * Integer.parseInt(e[1]);
                double gd2 = gd +  Double.parseDouble(e[2]);
                DecimalFormat df = new DecimalFormat("#.###");
                waktu_kompresi.setText(String.valueOf(df.format(gd2)+" detik"));
            }
            byteHasilKompresi = gambar;
            lokasi_kompresi.setText(Config.WAVELET_STORAGE);
            ukuran_kompresi.setText(Utility.getInstance().convertByte(gambar.length));
            rasio_kompresi.setText(Utility.getInstance().rasioKompresi(ukuranGambarAsli,gambar.length));
            dashboard.collapse();
            dialog.dismiss();
        }

        @Override
        public void error(String error) {

            Toast.makeText(getContext(),error,Toast.LENGTH_SHORT).show();
            dialog.dismiss();
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == Config.RESULT_LOAD_IMAGE_REQUEST_CODE && resultCode == RESULT_OK && null != data) {

            Uri selectedImageUri = data.getData();
            String imagepath = Utility.getInstance().getPath(selectedImageUri,getActivity());
            imageName = imagepath.substring(imagepath.lastIndexOf("/"));
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmp = BitmapFactory.decodeFile(imagepath, bmOptions);
            File file = new File(imagepath);
            ukuranGambarAsli = bmp.getWidth()* bmp.getHeight()*3;
            String frmt = imageName.substring(imageName.lastIndexOf("."));
            String p = String.valueOf(bmp.getHeight());
            String l = String.valueOf(bmp.getWidth());

            if(!frmt.equals(".bmp")){
                rasio_asli_txt.setText("Ukuran gambar "+frmt.substring(1,frmt.length()));
                rasio_asli.setText(Utility.getInstance().convertByte(file.length())+" rasio = "+Utility.getInstance().rasioKompresi(ukuranGambarAsli,file.length()));
                rasioAsli(View.VISIBLE,View.VISIBLE);
            }else{
                rasioAsli(View.GONE,View.GONE);
            }
            setImageOriInformation(p,l,imagepath,frmt);
            visibleImage(View.VISIBLE,View.GONE,View.GONE);
        }
    }

    private void rasioAsli(int txt, int rtxt){
        rasio_asli_txt.setVisibility(txt);
        rasio_asli.setVisibility(rtxt);
    }
    public void doKompres(){
        if(bmp==null){
            openGallerry();
            Toast.makeText(getActivity(),"Silahkan pilih gambar yang akan dikompresi terlebih dahulu",Toast.LENGTH_LONG).show();
        }else{
            doHaar();
        }
    }






    private void doHaar(){
        dialog.show();
        timestart= System.currentTimeMillis();
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    Haar haarKompresi = new Haar();

                    haarKompresi.kompres(bmp, callback, getActivity());
                }catch (OutOfMemoryError io){
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(), "silahkan restart aplikasi terlebih dahulu", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    });
                }
            }
        };

        thread.start();

    }

    public boolean simpanFile(){
        if(byteHasilKompresi==null){
            Toast.makeText(getActivity(),"Tidak ada file yang bisa disimpan, Silahkan melakukan proses kompresi terlebih dahulu",Toast.LENGTH_SHORT).show();
              return false;
        }
        return true;
    }

    public String getImageName(){

        return imageName.substring(1,imageName.lastIndexOf("."));
    }

    public void simpanCode(final String filename){
        dialog.show();
        Thread thread = new Thread() {
            @Override
            public void run() {

                try {
                    File direct = new File(Config.PATH_KOMPRESI);

                    if (!direct.exists()) {
                        File wallpaperDirectory = new File(Config.PATH_FILE);
                        wallpaperDirectory.mkdirs();
                    }

                    File file = new File(new File(Config.PATH_FILE), filename+Config.EXTENSION);
                    if (file.exists()) {
                        file.delete();
                    }
                    try {
                        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
                        bos.write(byteHasilKompresi);
                        bos.flush();
                        bos.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateStorage("/" + filename + Config.EXTENSION);
                            Toast.makeText(getActivity(), "File telah disimpan", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    });
                } catch (Exception e) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(),"Tidak dapat menyimpan gambar",Toast.LENGTH_SHORT).show();

                            dialog.dismiss();
                        }
                    });

                    e.printStackTrace();
                }
            }
        };

        thread.start();
    }

    private void updateStorage(String filename){
        lokasi_kompresi.setText(Config.WAVELET_STORAGE+filename);
    }
    private void setImageOriInformation(String pnjng, String lbr, String path,String formattxt){
        panjang.setText(Utility.getInstance().convertDot(pnjng));
        lebar.setText(Utility.getInstance().convertDot(lbr));
        lokasi.setText(path);
        ukuran.setText(Utility.getInstance().convertByte(ukuranGambarAsli));
        format.setText(formattxt);
        gmbar.setImageBitmap(bmp);
    }

    private void intentGallery(){
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, Config.RESULT_LOAD_IMAGE_REQUEST_CODE);

    }

    private void visibleImage(int img, int choose, int zom){
        gmbar.setVisibility(img);
        chooseImg.setVisibility(choose);
        zoom.setVisibility(zom);
    }

    private void reset(){
        panjang.setText("-");
        lebar.setText("-");
        lokasi.setText("-");
        ukuran.setText("-");
        lokasi_kompresi.setText("-");
        ukuran_kompresi.setText("-");
        waktu_kompresi.setText("-");
        rasio_kompresi.setText("-");
        format.setText("-");
        visibleImage(View.GONE, View.VISIBLE,View.GONE);
        rasioAsli(View.GONE,View.GONE);
        imageName="";
        ukuranGambarAsli=0;
        timestart=0;
        bmp = null;
        byteHasilKompresi = null;
        dashboard.collapse();
    }
        @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.reset, menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        int id=item.getItemId();

        if(id==R.id.reset) {
            reset();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
