package ericnataa.skripsi.model;

/**
 * Created by User on 1/27/2018.
 */

public class BandingData {
    private String x;
    private String y;
    private String redAsli;
    private String greenAsli;
    private String blueAsli;
    private String redHaar;
    private String greenHaar;
    private String blueHaar;

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y;
    }

    public String getRedAsli() {
        return redAsli;
    }

    public void setRedAsli(String redAsli) {
        this.redAsli = redAsli;
    }

    public String getGreenAsli() {
        return greenAsli;
    }

    public void setGreenAsli(String greenAsli) {
        this.greenAsli = greenAsli;
    }

    public String getBlueAsli() {
        return blueAsli;
    }

    public void setBlueAsli(String blueAsli) {
        this.blueAsli = blueAsli;
    }

    public String getRedHaar() {
        return redHaar;
    }

    public void setRedHaar(String redHaar) {
        this.redHaar = redHaar;
    }

    public String getGreenHaar() {
        return greenHaar;
    }

    public void setGreenHaar(String greenHaar) {
        this.greenHaar = greenHaar;
    }

    public String getBlueHaar() {
        return blueHaar;
    }

    public void setBlueHaar(String blueHaar) {
        this.blueHaar = blueHaar;
    }
}
